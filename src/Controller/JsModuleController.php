<?php

namespace Drupal\js_module\Controller;

/**
 * Controller routines for js block routes.
 */
class JsModuleController {
  

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'js_module';
  }

}
